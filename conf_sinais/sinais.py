import time
from datetime import datetime, timedelta
from dateutil import tz


def timestamp_converter(x, retorno=1):
    hora = datetime.strptime(datetime.utcfromtimestamp(x + 0).strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    hora = hora.replace(tzinfo=tz.gettz('GMT'))

    return str(hora.astimezone(tz.gettz('America/Sao Paulo')))[:-6] if retorno == 1 else hora.astimezone(
        tz.gettz('America/Sao Paulo'))


def timestamp_calculate(dia):
    time_dias = timedelta(days=dia).total_seconds()
    data = time.time() + time_dias
    dia_conv = timestamp_converter(data)
    return data, dia_conv


def sep_for_days():
    arquivo = open('sinais_today.txt', 'w')
    arquivo.close()
    arquivo = open('sinais_today.txt', 'r')
    conteudo = arquivo.readlines()

    listando_sinais = open('/home/whyl/bot-market-financial/gerador-de-sinais/sinais/sinais_totais.txt', 'r')
    lista_sinal = listando_sinais.readlines()
    listando_sinais.close()

    arquivo = open('/home/whyl/bot-market-financial/gerador-de-sinais/sinais/sinais_totais.txt', 'r')
    arquivo2 = open('/home/whyl/bot-market-financial/gerador-de-sinais/sinais.txt', 'r')
    list_sinal = arquivo2.readlines()
    arquivo2.close()


    # try:
    for i in lista_sinal:
        c = arquivo.readline()
        c = c.split()


        line_sig = str(c[5] + ' ' + c[6])
        arquivo2 = open('/home/whyl/bot-market-financial/gerador-de-sinais/sinais.txt', 'r')
        list_sinal = arquivo2.readlines()
        arquivo2.close()
        arquivo2 = open('/home/whyl/bot-market-financial/gerador-de-sinais/sinais.txt', 'r')


        # if  line_sig == str(d[2]):
        #     print(c,'\n' ,d)
        # print(c[5] +' ' + c[6],'-----' ,d[2])

        # ['id:', '291786', 'vela:', 'GREEN', 'data:', '2021-09-28', '08:30:00', 'GOAL:', 'EURUSD', '1632828600']
        # ['5', 'EURGBP', '2021-09-28 08:30:00', 'PUT\n']

        for x in list_sinal:
            d = arquivo2.readline()
            d = d.split(',')
            sig_line = str(d[2])
            if d[1] == c[-2]:
                w = 'PUT' if c[3] == 'RED' else 'CALL'
                y = 'PUT' if d[3] == 'PUT\n' else 'CALL'
                if line_sig == sig_line and w == y:
                    conteudo.append(f'{d[1]}, {d[2]}, {y} ')
                    conteudo.append('-- WIN -- \n')

                    print(f'-- WIN -- {d[1]}, {d[2]}, {y} \n')
                else:
                    if line_sig == sig_line:
                        conteudo.append(f'{d[1]}, {d[2]}, {y} ')
                        conteudo.append('-- LOOSE -- \n')

                        print(f'-- LOOSE -- {d[1]}, {d[2]}, {y} \n')

            # if line_sig == sig_line and w != y:
            #     print(f'{d[1]}, {d[2]}, {d[3]} -- LOOSE')
                # print( c[3] , d[3])

    # except IndexError:

    arquivo = open('sinais_today.txt', 'w')
    arquivo.writelines(conteudo)
    arquivo.close()


# sep_for_days()

# def finish_signals():
#     arquivo = open('sinais_today.txt', 'w')
#     arquivo.close()
#
#     arquivo = open('sinais/trantando_sinais.txt', 'r')
#     lista_sinal = arquivo.readlines()
#     tam_list = int(len(lista_sinal))
#     arquivo.close()
#
#     arquivo = open('sinais/trantando_sinais.txt', 'r')
#     singnal_finish = open('sinais_today.txt', 'r')
#     conteudo = singnal_finish.readlines()
#
#     for candle_goal in goal:
#         for y in range(int(tam_list / len(goal))):
#             try:
#                 c = arquivo.readline()
#                 c = c.split()
#
#                 green = c.count('GREEN')
#                 red = c.count('RED')
#                 trat_green = "%.2f" % ((green / dias) * 100)
#                 trat_red = "%.2f" % ((red / dias) * 100)
#                 tmdata, dia_conv = timestamp_calculate(1)
#                 tmdata2, days_todays = timestamp_calculate(0)
#                 day_today = days_todays.split()
#                 day_today = day_today[0]
#                 dias_conv = dia_conv
#                 dias_conv = dias_conv.split()
#                 dias_conv = dias_conv[0]
#                 hours_minutes = c[2]
#                 hours_minutes = str(hours_minutes.split())
#                 dia_sinal = c[1]
#                 # print(c)
#
#                 # print(day_today[8:], dia_sinal[8:], int(dias_conv[8:]) - 1)
#                 print(f'CALL:{trat_green}% || PUT: {trat_red}%  -  {time_candle}, {c[4]},{day_today} {c[2]} {c[3]}')
#                 if float(trat_green) >= taxa_acertividade and int(dia_sinal[8:]) < (int(dias_conv[8:]) - 1):
#                     conteudo.append(f'{time_candle},{candle_goal},{day_today} {c[2]},CALL\n')
#
#                 if float(trat_red) >= taxa_acertividade and int(dia_sinal[8:]) < (int(dias_conv[8:]) - 1):
#                     conteudo.append(f'{time_candle},{candle_goal},{day_today} {c[2]},PUT\n')
#
#                 if float(trat_green) >= taxa_acertividade and int(dia_sinal[8:]) >= (int(dias_conv[8:]) - 1):
#                     conteudo.append(f'{time_candle},{candle_goal},{dias_conv} {c[2]},CALL\n')
#
#                 if float(trat_red) >= taxa_acertividade and int(dia_sinal[8:]) >= (int(dias_conv[8:]) - 1):
#                     conteudo.append(f'{time_candle},{candle_goal},{dias_conv} {c[2]},PUT\n')
#
#             except IndexError:
#                 pass
#
#     singnal_finish = open('sinais_today.txt', 'w')
#     singnal_finish.writelines(conteudo)
#     singnal_finish.close()
#     arquivo.close()
#
#     print('Finished')
