import configparser
import config
import time
import signal
from iqoptionapi.stable_api import IQ_Option
from datetime import datetime
from dateutil import tz

def timeout(signum, frame):
    raise Exception('Restarting')
    while True:
        try:
            signal.signal(signal.SIGALRM, timeout)
        except:
            signal.signal(signal.SIGALRM, timeout)

error_password = """{"code":"invalid_credentials","message":"You entered the wrong credentials. Please check that the login/password is correct."}"""
API = IQ_Option(config.USER, config.PASSWORD)

check, reason = API.connect()

if check:
    print("Start your robot")
    # if see this you can close network for test
    while True:
        if API.check_connect() == False:  # detect the websocket is close
            print("try reconnect")
            check, reason = API.connect()
            if check:
                print("Reconnect successfully")
            else:
                if reason == error_password:
                    print(error_password)
                else:
                    print("No Network")
        else:
            print('Login feito com sucesso')
            break

else:

    if reason == "[Errno -2] Name or service not known":
        print("No Network")

    if reason == "2FA":
        print('##### 2FA HABILITADO #####')
        print("Um sms foi enviado com um código para seu número")

        code_sms = input("Digite o código recebido: ")
        status, reason = API.connect_2fa(code_sms)

        print('##### Segunda tentativa #####')
        print('Status:', status)
        print('Reason:', reason)
        print("Email:", API.email)

    elif reason == error_password:
        print("Error Password")

API.change_balance(config.MODE)

def banca():
    return float(API.get_balance())

balance_take = float(API.get_balance()) + float(config.TAKE)
balance_stop = float(API.get_balance()) - float(config.STOP)
balance_initial = float(API.get_balance())
print(f'Take: {("%.2f" %(balance_take))}')
print(f'Stop: {(("%.2f" %balance_stop))}')


def configuration():
    arquivo = configparser.RawConfigParser()
    arquivo.read('config.txt')

    return {'seguir_ids': arquivo.get('GERAL', 'seguir_ids'), 'ciclo': arquivo.get('GERAL', 'ciclo'), 'stop_win': arquivo.get('GERAL', 'stop_win'),
            'mhi': arquivo.get('GERAL', 'mhi'),'copy': arquivo.get('GERAL', 'copy'),'sinais': arquivo.get('GERAL', 'sinais'),
            'digitais': arquivo.get('GERAL', 'digitais'), 'tendencia': arquivo.get('GERAL', 'tendencia'),
            'stop_loss': arquivo.get('GERAL', 'stop_loss'), 'payout': 0, 'banca_inicial': banca(),
            'filtro_diferenca_sinal': arquivo.get('GERAL', 'filtro_diferenca_sinal'),'fator_gale': arquivo.get('GERAL', 'fator_gale'),
            'martingale': arquivo.get('GERAL', 'martingale'), 'proximo_sinal': arquivo.get('GERAL', 'proximo_sinal'),
            'sorosgale': arquivo.get('GERAL', 'sorosgale'), 'nivel_p_proximo_sinal': arquivo.get('GERAL', 'nivel_p_proximo_sinal'),
            'niveis': arquivo.get('GERAL', 'niveis'), 'filtro_pais': arquivo.get('GERAL', 'filtro_pais'),
            'filtro_top_traders': arquivo.get('GERAL', 'filtro_top_traders'),
            'valor_minimo': arquivo.get('GERAL', 'valor_minimo'), 'paridade': arquivo.get('GERAL', 'paridade'),
            'valor_entrada': arquivo.get('GERAL', 'valor_entrada'), 'timeframe': arquivo.get('GERAL', 'timeframe')}


def upload_signal():
    arquivo = open('gerador-de-sinais/sinais.txt', encoding='UTF-8')
    lista = arquivo.read()
    arquivo.close()

    lista = lista.split('\n')

    for index, a in enumerate(lista):
        if a == '':
            del lista[index]

    return lista


def timestamp_converter(x, retorno=1):
    hora = datetime.strptime(datetime.utcfromtimestamp(x + 1).strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    hora = hora.replace(tzinfo=tz.gettz('GMT'))

    return str(hora.astimezone(tz.gettz('America/Sao Paulo')))[:-6] if retorno == 1 else hora.astimezone(
        tz.gettz('America/Sao Paulo'))

print('Balance:', banca())
lista = upload_signal()
config_txt = configuration()


def gale(amout):
    value = amout * float(config_txt['fator_gale'])
    return value


def def_trend(active, time_op):
    par = str(active)
    timeframe = int(time_op)
    velas = API.get_candles(par, (int(timeframe) * 60), 14, time.time())
    ultimo = round(velas[0]['close'], 4)
    primeiro = round(velas[-1]['close'], 4)
    diferenca = abs(round(((ultimo - primeiro) / primeiro) * 100, 3))
    tendencia = "CALL" if ultimo < primeiro and diferenca > 0.01 else "PUT" if ultimo > primeiro and diferenca > 0.01 else False
    return tendencia


def def_gale_next_signal(amount, direct, timeframe, active, cicle, nivelsinal):

    while True:
        if check_balance() == True:
            break

        direction = (direct.upper())

        if config_txt['martingale'] == 'S':
            cicles = cicle + 1
            value = gale(amount)
            if cicles >= (int(config_txt['niveis']) + 1):
                if config_txt['proximo_sinal'] == 'S':
                    cicles_nivel = nivelsinal + 1
                    value = gale(amount)
                    if cicles_nivel >= (int(config_txt['nivel_p_proximo_sinal']) + 1):
                        sinais(config.AMOUNT, 0, 0)
                    else:
                        print(f'\nCiclo: {cicles_nivel}')
                        sinais(value, 0, cicles_nivel)
                sinais(config.AMOUNT, 0,nivelsinal)
            else:
                print(f'\nGale: {cicles}')
                start_operation(config.TYPEOPERATION, value, direction, timeframe, active, cicles,nivelsinal)

        else:
            start_operation(config.TYPEOPERATION, config.AMOUNT, direction, timeframe, active, 0, 0)
        return False


def start_operation_digital(amount, direct, timeframe, active, cicle,nivelsinal):

    while True:

        if check_balance() == True:
            break

        signal.signal(signal.SIGALRM, timeout)

        tempo_error = (timeframe * 60)

        direction = (direct.lower())
        payout_digital = API.get_digital_payout(active, timeframe)
        print(f'Payout de: {payout_digital}%')
        buy_check, id = API.buy_digital_spot_v2(active, amount, direction, timeframe)
        print("\nwait for check win")

        try:
            if id != "error":

                signal.alarm(tempo_error + 5)
                print(f'Tempo de erro {tempo_error}')

                while True:
                    check, win = API.check_win_digital_v2(id)
                    if check == True:
                        break
                signal.alarm(0)
                print('Dentro do timer')
                if win < 0:
                    print("you loss " + ("%.2f" % (win)) + "$")
                    def_gale_next_signal(amount,direction,timeframe,active,cicle,nivelsinal)
                else:
                    print("you win " + ("%.2f" % (win)) + "$\n")
                    sinais(config.AMOUNT, 0, 0)
            else:
                print("please try again")

        except TypeError:
            print('Ativo inativo.')
            if amount == config.AMOUNT:
                sinais(config.AMOUNT, 0, 0)
            else:
                sinais(amount, cicle, nivelsinal)

        except Exception:
            print('Fora do timer')
            while True:
                check, win = API.check_win_digital_v2(id)
                if check == True:
                    break
            if win < 0:
                print("you loss " + ("%.2f" % (win)) + "$")
                def_gale_next_signal(amount,direction,timeframe,active,cicle,nivelsinal)
            else:
                print("you win " + ("%.2f" % (win)) + "$\n")
                sinais(config.AMOUNT, 0, 0)
        else:
            print("please try again")


def start_operation_binary(amount, direct, timeframe, active, cicle,nivelsinal):

    while True:

        if check_balance() == True:
            break

        signal.signal(signal.SIGALRM, timeout)

        tempo_error = (timeframe * 60)

        direction = (direct.lower())
        buy_check, id = API.buy(amount, active, direct, timeframe)
        print("\nwait for check win")


        try:
            if id != "error":

                signal.alarm(tempo_error + 5)
                print(f'Tempo de erro {tempo_error}')

                while True:
                    result_op, win_money = API.check_win_v3(id)
                    if win_money:
                        break
                signal.alarm(0)
                print('Dentro do timer')
                # print(result_op, win_money)
                if result_op == 'win':
                    print("you win " + ("%.2f" % (win_money)) + "$\n")
                    sinais(config.AMOUNT, 0, 0)
                if result_op == 'loose':
                    print("you loss " + ("%.2f" % (win_money)) + "$")
                    def_gale_next_signal(amount, direction, timeframe, active, cicle, nivelsinal)
                else:
                    print("Draw" + ("%.2f" % (win_money)) + "$")
                    sinais(config.AMOUNT, 0, 0)
            else:
                print("please try again")
        except:
            if id != "error":
                print('Fora do timer')
                while True:
                    result_op, win_money = API.check_win_v3(id)
                    if win_money:
                        break
                # print(result_op, win_money)
                if result_op == 'win':
                    print("you win " + ("%.2f" % (win_money)) + "$\n")
                    sinais(config.AMOUNT, 0, 0)
                if result_op == 'loose':
                    print("you loss " + ("%.2f" % (win_money)) + "$")
                    def_gale_next_signal(amount, direction, timeframe, active, cicle, nivelsinal)
                else:
                    print("Draw" + ("%.2f" % (win_money)) + "$")
                    sinais(config.AMOUNT, 0, 0)
            else:
                print("please try again")


def start_operation(type_operation, amount, direct, timeframe, active, cicle,nivelsinal):
    if type_operation == "DIGITAL":
        start_operation_digital(amount, direct, timeframe, active, cicle,nivelsinal)

    if type_operation == "BINARY":
        start_operation_binary(amount, direct, timeframe, active, cicle,nivelsinal)


def sinais(amount, cicle, nivelsinal):

    while True:

        if check_balance() == True:
            break

        time_now = timestamp_converter(time.time())
        for sinal in lista:
            dados = sinal.split(',')
            # print(time_now)
            if time_now == dados[2]:
                trend = def_trend(dados[1], dados[0])
                # config['payout'] = float(payout(dados[1], entering, int(dados[0])) / 100)
                print('\n', dados)
                if trend != dados[3] and config_txt['tendencia'] == 'S':
                    if trend == False:
                        print('Tendência não definida, aguardando próximo sinal.')
                        sinais(amount, cicle, nivelsinal)
                    print('\nFora de tendência, aguardando próximo sinal.\nTendência de:', trend)
                    # time.sleep(5)
                    print(time_now)
                    sinais(amount, cicle,nivelsinal)
                else:
                    print(f'\n{time_now}\n')
                    print('Tendência de:', trend)
                    start_operation(config.TYPEOPERATION, amount, dados[3], int(dados[0]), dados[1], cicle,nivelsinal)
                    if check_balance() == True:
                        break


def check_balance():

    if banca() >= balance_take:
        print('Take Profit')
        return True
    if banca() <= balance_stop:
        print('Stop Loss')
        return True
    else:
        return False


def starting():
    while True:
        if check_balance() == True:
            break
        try:
            # signal.alarm(5)
            signal.signal(signal.SIGALRM, timeout)
            signal.alarm(5)
            print('Inicializing')
            # print('Deu certo')
            sinais(config.AMOUNT, 0, 0)
            # signal.alarm(0)
        # except Exception as e:
        #     print(e)
        except:
            sinais(config.AMOUNT, 0, 0)


if __name__ == '__main__':
    starting()
